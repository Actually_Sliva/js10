"use strict"

const tabs = document.querySelector(".tabs");                       
const tabsContent = document.querySelector(".tabs-content");

function isVisibleContent() {
    [...tabsContent.children].forEach(element => {              
    let activeLi = document.querySelector(".active");         
        if (element.dataset.tab === activeLi.dataset.tab) {   
            element.style.display = "block";                  
        } else {                                              
            element.style.display = "none";                   
        }
    });
}

isVisibleContent();

tabs.addEventListener("click", onClickTabs);                

function onClickTabs(event) {
    let target = event.target;
    let oldActive = document.querySelector(".active");
    oldActive.classList.remove("active");
    target.closest("li").classList.add("active");

    [...tabsContent.children].forEach(element => {
        if (target.dataset.tab === element.dataset.tab) {    
        element.style.display = "block";
        } else {element.style.display = "none";}
    });
}


